<?php get_header(); ?>

<main role="main" class="layoutMain">
    <?php if (have_posts()): while (have_posts()): the_post(); ?>
        <article class="layoutMain__article">
            <header>
                <h1><?php the_title(); ?></h1>
            </header>

            <?php the_content(); ?>
        </article>
    <?php endwhile; else: ?>
        <article class="layoutMain__article">
            <p><?php pll_e("Oops, il n'y a rien ici :("); ?></p>
        </article>
    <?php endif; ?>
</main>

<?php get_footer();
