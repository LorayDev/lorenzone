<?php

if (function_exists('pll_register_string')) {
    pll_register_string('at', 'à', 'Theme Basics');

    pll_register_string('weeklives', 'Mes lives de la semaine', 'Theme Widgets');
    pll_register_string('noweeklives', 'Pas de lives cette semaine', 'Theme Widgets');
    pll_register_string('lastvideo', 'Ma dernière vidéo', 'Theme Widgets');
    pll_register_string('socials', 'Mes réseaux sociaux', 'Theme Widgets');
    pll_register_string('weekevents', 'Événements de la semaine', 'Theme Widgets');
    pll_register_string('contacts', 'Mes coordonnées', 'Theme Widgets');
    pll_register_string('currently', 'Actuellement...', 'Theme Widgets');
    pll_register_string('creatingawebsite', 'en création de site', 'Theme Widgets');
    pll_register_string('writingablogpost', 'en rédaction d\'article', 'Theme Widgets');
    pll_register_string('liveon', 'en live sur', 'Theme Widgets');
    pll_register_string('live', 'en live', 'Theme Widgets');
    pll_register_string('photovideoediting', 'en montage photo / vidéo', 'Theme Widgets');
    pll_register_string('businessrelationship', 'en train de faire du relationnel', 'Theme Widgets');
    pll_register_string('lifeliving', 'en train de vivre ma vie', 'Theme Widgets');
    pll_register_string('weekposts', 'Publications prévues cette semaine', 'Theme Widgets');

    pll_register_string('sitemap', 'Plan du site', 'Theme Footer');
}