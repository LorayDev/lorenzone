<?php
$current_language = pll_current_language();
$post_thumbnail = '-noThumbnail';
$categories_arg = [
    'hide_empty' => true,
    'show_count' => true
];
get_header(); ?>

<main role="main" class="layoutMain <?php echo $post_thumbnail; ?>">
    <article class="layoutMain__article">
        <?php if (have_posts()){
            $t = 0;
            while (have_posts()) {
                the_post();
                $t++;
            }
        } ?>
        <?php if (have_posts()): ?>
        <ul class="m-postCards <?php if ($t == 8): ?>-grid<?php else:?>-archive<?php endif;?>">
                <?php $k = 0; ?>
                <?php while (have_posts()): the_post(); ?>
                    <?php $image = get_the_post_thumbnail_url($post->ID); ?>
                        <li class="a-postCard
                        <?php if ($k == 0 || $k == 7) { echo "-large"; } else { echo "-small"; } ?>
                        <?php if ($k != 0 && ($k + 1) % 3 == 0 ) { echo "-alt"; }?>">
                            <a class="a-postCard__link" href="<?php echo get_permalink($post->ID); ?>">
                                <header class="a-postCard__header">
                                    <div class="a-postCard__image" style="background-image: url(<?php echo $image; ?>)">
                                    </div>
                                </header>
                                <main class="a-postCard__main">
                                    <p class="a-postCard__title">
                                        <?php echo $post->post_title; ?>
                                    </p>
                                    <p class="a-postCard__excerpt">
                                        <?php echo $post->post_excerpt; ?>
                                    </p>
                                </main>
                            </a>
                            <footer class="a-postCard__footer">
                            <ul class="a-postCard__meta">
                                <li class="a-postCard__date"><?php echo $post->post_date; ?></li>
                                <li class="a-postCard__category">
                                    <a href="#" class="a-postCard__categoryLink">Design</a></li>
                            </ul>
                            </footer>
                        </li>
                    <?php $k++; ?>
                <?php endwhile; ?>
            </ul>
            <?php theme_pagination(); ?>
        <?php else: ?>
                <p><?php pll_e("Oops, il n'y a rien ici :("); ?></p>
        <?php endif; ?>
    </article>
    <aside class="layoutMain__aside">
        <?php wp_list_categories($categories_arg); ?>
    </aside>
</main>

<?php get_footer();
