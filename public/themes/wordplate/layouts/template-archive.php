<?php
/*
Template Name: Archive Layout
*/
?>

<?php
$current_language = pll_current_language();
$post_thumbnail = '-noThumbnail';
get_header(); ?>

<?php if (get_the_post_thumbnail()): ?>
<?php $post_thumbnail = '-thumbnail'; ?>
<div class="layoutMain__thumbnail" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
</div>
<?php endif; ?>

<main role="main" class="layoutMain <?php echo $post_thumbnail; ?>">
    <aside class="layoutMain__aside">
        <?php $taxonomies = get_taxonomies(); ?>
        <pre><?php var_dump($taxonomies); ?></pre>
    </aside>
    <?php if (have_posts()): while (have_posts()): the_post(); ?>

        <article class="layoutMain__article">
            <?php the_content(); ?>
            <?php if (is_front_page()): 
                $recent_posts_args = [
                    'numberposts' => 8,
                    'post_status' => 'publish'
                ];
                $recent_posts = wp_get_recent_posts( $recent_posts_args, ARRAY_A );
                ?>
            <ul class="m-postCards -grid">
                <?php foreach ($recent_posts as $k => $post): 
                    $image = get_the_post_thumbnail_url($post['ID']); ?>
                    <li class="a-postCard 
                    <?php if ($k == 0 || $k == 7) { echo "-large"; } else { echo "-small"; } ?>
                    <?php if ($k != 0 && ($k + 1) % 3 == 0 ) { echo "-alt"; }?>">
                        <a class="a-postCard__link" href="<?php echo get_permalink($post['ID']); ?>">
                            <header class="a-postCard__header">
                                <div class="a-postCard__image" style="background-image: url(<?php echo $image; ?>)">
                                </div>
                            </header>
                            <main class="a-postCard__main">
                                <p class="a-postCard__title">
                                    <?php echo $post['post_title']; ?>
                                </p>
                                <p class="a-postCard__excerpt">
                                    <?php echo $post['post_excerpt']; ?>
                                </p>
                            </main>
                        </a>
                        <footer class="a-postCard__footer">
                        <ul class="a-postCard__meta">
                            <li class="a-postCard__date">Il y a 1 mois</li>
                            <li class="a-postCard__category"><a href="#" class="a-postCard__categoryLink">Design</a></li>
                        </ul>
                        </footer>
                    </li>
                <?php endforeach; ?>
            </ul>

            <?php endif; ?>

        </article>
    <?php endwhile; else: ?>
        <article class="layoutMain__article">
            <p><?php pll_e("Oops, il n'y a rien ici :("); ?></p>
        </article>
    <?php endif; ?>
</main>

<?php get_footer();
