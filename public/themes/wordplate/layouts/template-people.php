<?php
/*
Template Name: People Layout
*/
?>

<?php
$current_language = pll_current_language();
$post_thumbnail = '-noThumbnail';
get_header(); ?>

<main role="main" class="layoutMain <?php echo $post_thumbnail; ?>">
    <?php if (have_posts()): while (have_posts()): the_post(); ?>

        <article class="layoutMain__article">
            <header>
                <h1><?php the_title(); ?></h1>
            </header>

            <?php the_content(); ?>

            <?php if (have_rows('profile_cards')): ?>
                <ul class="m-profileCards">
                    <?php while (have_rows('profile_cards')): the_row(); ?>
                    <?php
                    $link = get_sub_field('link');
                    $image = get_sub_field('image'); ?>
                        <li class="a-profileCard">
                            <header class="a-profileCard__header">
                                <a
                                    class="a-profileCard__link"
                                    href="<?php echo $link['url']; ?>"
                                    target="_blank">
                                    <img
                                        class="a-profileCard__img"
                                        src="<?php echo $image['url']; ?>"
                                        alt="<?php echo $image['alt']; ?>">
                                </a>
                            </header>
                            <main class="a-profileCard__main">
                                <p class="a-profileCard__name"><?php the_sub_field('name'); ?></p>
                                <p class="a-profileCard__job"><?php the_sub_field('job'); ?></p>
                                <p class="a-profileCard__url">
                                    <a
                                        class="a-profileCard__link"
                                        href="<?php echo $link['url']; ?>"
                                        target="_blank"><?php echo $link['title']; ?>
                                    </a>
                                </p>
                            </main>
                        </li>
                    <?php endwhile; ?>
                </ul>
            <?php endif; ?>

        </article>
    <?php endwhile; else: ?>
        <article class="layoutMain__article">
            <p><?php pll_e("Oops, il n'y a rien ici :("); ?></p>
        </article>
    <?php endif; ?>
</main>

<?php get_footer();