<?php
/*
Template Name: Side Layout
*/
?>

<?php
$current_language = pll_current_language();
$post_thumbnail = '-noThumbnail';
get_header(); ?>

<?php if (get_the_post_thumbnail()): ?>
<?php $post_thumbnail = '-thumbnail'; ?>
<div class="layoutMain__thumbnail" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
</div>
<?php endif; ?>

<main role="main" class="layoutMain <?php echo $post_thumbnail; ?>">
    <aside class="layoutMain__aside">
        <?php if (have_rows('about', 'options')): ?>
        <div class="m-aside -about">
            <?php $about_image = get_field('about_image', 'options'); ?>
            <img class="m-aside__aboutImage" src="<?php echo $about_image['url']; ?>" alt="<?php echo $about_image['alt']?>">
            <?php while (have_rows('about', 'options')): the_row(); ?>
                <?php $about = get_sub_field('zone_translations'); ?>
                    <?php foreach ($about as $a): ?>
                        <?php if ($current_language === $a['language']): ?>
                            <?php echo $a['text']; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
        <?php if( have_rows('side_elements') ): ?>

            <?php while ( have_rows('side_elements') ) : the_row(); ?>
                <?php if( get_row_layout() == 'lives' ): ?>
                    <div class="m-aside">
                        <p class="m-aside__title"><?php pll_e('Mes lives de la semaine'); ?></p>
                        <?php if( have_rows('lives_week', 'options') ): ?>
                            <ul class="a-livesTable">
                            <?php while ( have_rows('lives_week', 'options') ): the_row(); ?>
                                <?php
                                        $date = strtotime(get_sub_field('date_time'));
                                ?>
                                <?php if ($current_language === 'fr'): setlocale(LC_TIME, 'fr_FR.utf8','fra'); ?>
                                    <li class="a-livesTable__day">
                                        <p class="a-livesTable__date -day">
                                            <?php echo strftime('%A %d', $date); ?>
                                        </p>
                                        <p class="a-livesTable__date -hour">
                                            <?php echo date('H:i', $date); ?>
                                        </p>
                                    </li>
                                <?php else: ?>
                                    <li class="a-livesTable__day">
                                        <p class="a-livesTable__date -day">
                                            <?php echo date('l jS', $date); ?>
                                        </p>
                                        <p class="a-livesTable__date -hour">
                                            <?php echo date('g:i a', $date); ?>
                                        </p>
                                    </li>
                                <?php endif; ?>
                            <?php endwhile; ?>
                            </ul>
                        <?php else: ?>
                            <p class="m-aside__information -lives"><?php pll_e('Pas de lives cette semaine'); ?></p>
                        <?php endif; ?>
                    </div>

                <?php elseif( get_row_layout() == 'videos' ): ?>
                    <div class="m-aside">
                        <p class="m-aside__title"><?php pll_e('Ma dernière vidéo'); ?></p>
                    </div>
                <?php elseif( get_row_layout() == 'socials' ): ?>
                    <div class="m-aside">
                        <p class="m-aside__title"><?php pll_e('Mes réseaux sociaux'); ?></p>
                        <?php if( have_rows('socials', 'options') ): ?>
                            <ul class="a-socialsTable">
                            <?php while ( have_rows('socials', 'options') ): the_row(); ?>
                                <?php
                                    $image = get_sub_field('image');
                                    $link = get_sub_field('link');
                                    ?>
                                <li class="a-socialsTable__socialElement">
                                    <?php if ($link && $image): ?>
                                        <a class="a-socialsTable__socialLink" href="<?= $link ?>" target="_blank">
                                            <img class="a-socialsTable__socialImage" src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                                        </a>
                                    <?php endif; ?>
                                </li>
                            <?php endwhile; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                <?php elseif( get_row_layout() == 'events' ): ?>
                    <div class="m-aside">
                        <p class="m-aside__title"><?php pll_e('Événements de la semaine'); ?></p>
                        <ul class="m-incomingElements -side">
                            <?php while ( have_rows('evenements_a_venir', 'options') ): the_row(); ?>
                                <li class="a-incoming__container">
                                    <p class="a-incoming"> <span class="a-incoming__title"><?php the_sub_field('titre'); ?></spans> - <span class="a-incoming__date"><?php the_sub_field('prevu_pour'); ?></span></p>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                <?php elseif( get_row_layout() == 'contacts' ): ?>
                    <div class="m-aside">
                        <p class="m-aside__title"><?php pll_e('Mes coordonnées'); ?></p>
                        <?php if( have_rows('contacts', 'options') ): ?>
                            <ul class="m-contacts">
                            <?php while ( have_rows('contacts', 'options') ): the_row(); ?>
                                <?php if ( get_row_layout() == 'phone' ): ?>
                                    <li class="a-contact">
                                        <strong>
                                        <?php while ( have_rows('string_translations') ): the_row(); ?>
                                            <?php if (get_sub_field('language') === $current_language): ?>
                                                <?php the_sub_field('text'); ?>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                        </strong>: <?php the_sub_field('numero'); ?>
                                </li>
                                <?php elseif ( get_row_layout() == 'e-mail' ): ?>
                                    <li class="a-contact">
                                        <strong>
                                        <?php while ( have_rows('string_translations') ): the_row(); ?>
                                            <?php if (get_sub_field('language') === $current_language): ?>
                                                <?php the_sub_field('text'); ?>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                        </strong>: <?php the_sub_field('adresse'); ?>
                                    </li>
                                <?php elseif ( get_row_layout() == 'adresse' ): ?>
                                    <li class="a-contact">
                                        <strong>
                                        <?php while ( have_rows('string_translations') ): the_row(); ?>
                                            <?php if (get_sub_field('language') === $current_language): ?>
                                                <?php the_sub_field('text'); ?>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                        </strong>:
                                        <?php $lignes = get_sub_field('lignes'); ?>
                                        <?php foreach ( $lignes as $ligne ): ?>
                                            <p>
                                                <?php echo $ligne['ligne']; ?>
                                            </p>
                                        <?php endforeach; ?>
                                            <p>
                                                <?php the_sub_field('code_postal'); ?>, <?php the_sub_field('ville'); ?>
                                            </p>
                                    </li>
                                <?php elseif ( get_row_layout() == 'lien' ): ?>
                                        <?php while ( have_rows('link_translations') ): the_row(); ?>
                                            <?php if (get_sub_field('language') === $current_language): ?>
                                                <?php $link = get_sub_field('link'); ?>
                                                <li class="a-contact">
                                                    <a class="a-contact__link" target="<?php echo $link['target']; ?>" href="<?php echo $link['url']; ?>">
                                                        <?php echo $link['title']; ?>
                                                    </a>
                                                </li>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                <?php endif; ?>
                            <?php endwhile; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                <?php elseif( get_row_layout() == 'now' ): ?>
                    <div class="m-aside -current">
                        <?php $currently = get_field('currently', 'options'); ?>
                        <?php if ($currently == 'tra'): ?>
                            <p class="m-aside__title -current"><?php pll_e('Actuellement...'); ?> <strong><?php pll_e('en création de site'); ?></strong></p>
                        <?php elseif ($currently == 'com1'): ?>
                            <p class="m-aside__title -current"><?php pll_e('Actuellement...'); ?> <strong><?php pll_e('en rédaction d\'article'); ?></strong></p>
                        <?php elseif ($currently == 'com2'): ?>
                            <?php if (get_field('currently_link', 'options') && get_field('currently_platform', 'options')): ?>
                                <p  class="m-aside__title -current"><?php pll_e('Actuellement...'); ?> <?php pll_e('en live sur'); ?> 
                                    <a class="m-aside__title -currentLink" href="<?php echo get_field('currently_link', 'options'); ?>" target="_blank">
                                        <?php echo get_field('currently_platform', 'options'); ?>
                                    </a>
                                </p>
                            <?php else: ?>
                                <p class="m-aside__title -current"><?php pll_e('Actuellement...'); ?> <strong><?php pll_e('en live'); ?></strong></p>
                            <?php endif; ?>
                        <?php elseif ($currently == 'com3'): ?>
                            <p class="m-aside__title -current"><?php pll_e('Actuellement...'); ?> <strong><?php pll_e('en montage photo / vidéo'); ?></strong></p>
                        <?php elseif ($currently == 'pro'): ?>
                            <p class="m-aside__title -current"><?php pll_e('Actuellement...'); ?> <strong><?php pll_e('en train de faire du relationnel'); ?></strong></p>
                        <?php elseif ($currently == 'more'): ?>
                            <?php if ( have_rows('string_translations', 'options') ):?>
                                <?php while ( have_rows('string_translations', 'options') ): the_row(); ?>
                                    <?php if (get_sub_field('language') === $current_language): ?>
                                        <p class="m-aside__title -current"><?php pll_e('Actuellement...'); ?> <strong><?php the_sub_field('text'); ?></stron></p>
                                    <?php endif; ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <p><?php pll_e('Actuellement...'); ?> <?php pll_e('en train de vivre ma vie'); ?></p>
                        <?php endif; ?>
                    </div>
                <?php elseif( get_row_layout() == 'posts' ): ?>
                    <div class="m-aside">
                        <?php if ($current_language === 'fr'): ?>
                            <?php if( have_rows('articles_a_venir', 'options') ): ?>
                                <p class="m-aside__title"><?php pll_e('Publications prévues cette semaine'); ?></p>
                                <ul class="m-incomingElements -side">
                                    <?php while ( have_rows('articles_a_venir', 'options') ): the_row(); ?>
                                        <li class="a-incoming__container">
                                            <p class="a-incoming"> <span class="a-incoming__title"><?php the_sub_field('titre'); ?></spans> - <span class="a-incoming__date"><?php the_sub_field('prevu_pour'); ?></span></p>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php if( have_rows('posts_to_come', 'options') ): ?>
                                <p class="m-aside__title"><?php pll_e('Publications prévues cette semaine'); ?></p>
                                <ul class="m-incomingElements -side">
                                    <?php while ( have_rows('posts_to_come', 'options') ): the_row(); ?>
                                        <li class="a-incoming__container">
                                            <p class="a-incoming"> <span class="a-incoming__title"><?php the_sub_field('titre'); ?></spans> - <span class="a-incoming__date"><?php the_sub_field('prevu_pour'); ?></span></p>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            <?php endwhile; ?>

        <?php endif; ?>
    </aside>
    <?php if (have_posts()): while (have_posts()): the_post(); ?>

        <article class="layoutMain__article">
            <?php the_content(); ?>
            <?php if (is_front_page()): 
                $recent_posts_args = [
                    'numberposts' => 8,
                    'post_status' => 'publish'
                ];
                $recent_posts = wp_get_recent_posts( $recent_posts_args, ARRAY_A );
                ?>
            <ul class="m-postCards -grid">
                <?php foreach ($recent_posts as $k => $post): 
                    $image = get_the_post_thumbnail_url($post['ID']); ?>
                    <li class="a-postCard 
                    <?php if ($k == 0 || $k == 7) { echo "-large"; } else { echo "-small"; } ?>
                    <?php if ($k != 0 && ($k + 1) % 3 == 0 ) { echo "-alt"; }?>">
                        <a class="a-postCard__link" href="<?php echo get_permalink($post['ID']); ?>">
                            <header class="a-postCard__header">
                                <div class="a-postCard__image" style="background-image: url(<?php echo $image; ?>)">
                                </div>
                            </header>
                            <main class="a-postCard__main">
                                <p class="a-postCard__title">
                                    <?php echo $post['post_title']; ?>
                                </p>
                                <p class="a-postCard__excerpt">
                                    <?php echo $post['post_excerpt']; ?>
                                </p>
                            </main>
                        </a>
                        <footer class="a-postCard__footer">
                        <ul class="a-postCard__meta">
                            <li class="a-postCard__date">Il y a 1 mois</li>
                            <li class="a-postCard__category"><a href="#" class="a-postCard__categoryLink">Design</a></li>
                        </ul>
                        </footer>
                    </li>
                <?php endforeach; ?>
            </ul>

            <?php endif; ?>

        </article>
    <?php endwhile; else: ?>
        <article class="layoutMain__article">
            <p><?php pll_e("Oops, il n'y a rien ici :("); ?></p>
        </article>
    <?php endif; ?>
</main>

<?php get_footer();
