<?php
$post_thumbnail = '-noThumbnail';
get_header(); ?>

<?php if (get_the_post_thumbnail()): ?>
<?php $post_thumbnail = '-thumbnail'; ?>
<div class="layoutMain__thumbnail" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
</div>
<?php endif; ?>

<main role="main" class="layoutMain <?php echo $post_thumbnail; ?>">
    <?php if (have_posts()): while (have_posts()): the_post(); ?>
        <article class="layoutMain__article o-post">
            <header class="o-post__header">
                <h1 class="o-post__title"><?php the_title(); ?></h1>
            </header>

            <?php the_content(); ?>
        </article>
    <?php endwhile; else: ?>
        <article class="layoutMain__article">
            <p><?php pll_e("Oops, il n'y a rien ici :("); ?></p>
        </article>
    <?php endif; ?>
</main>

<?php get_footer();
